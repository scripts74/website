<!DOCTYPE html>
<!-- Last Published: Sun Dec 04 2022 10:14:49 GMT+0000 (Coordinated Universal Time) -->
<html data-wf-domain="www.alexandrbashinsky.com" data-wf-page="638575c8e36e1bf082ab00a9" data-wf-site="638575c7e36e1bc9bcab00a5" lang="en">

<head>
	<meta charset="utf-8" />
	<title>Alexandr Bashinsky</title>
	<meta content="Alexandr Bashinsky" property="og:title" />
	<meta content="Alexandr Bashinsky" property="twitter:title" />
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<link href="../images/css/main.css" rel="stylesheet" type="text/css" />
	<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif]-->
	<script type="text/javascript">
	! function(o, c) {
		var n = c.documentElement,
			t = " w-mod-";
		n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
	}(window, document);
	</script>
	<link href="../images/favicon.png" rel="shortcut icon" type="image/x-icon" />
	<link href="../images/favicon2x.png" rel="apple-touch-icon" />
	<style>
	* {
		-webkit-font-smoothing: antialiased;
	}
	</style>
</head>

<body>
	<div data-w-id="77a9cd61-24e6-6e56-0a8a-b16b1e09cbeb" style="display:flex" class="preloader">
		<div class="preloader-left-div">
			<div style="-webkit-transform:translate3d(0, 150%, 0) scale3d(1, 1, 1) rotateX(90deg) rotateY(null) rotateZ(30deg) skew(0, 0);-moz-transform:translate3d(0, 150%, 0) scale3d(1, 1, 1) rotateX(90deg) rotateY(null) rotateZ(30deg) skew(0, 0);-ms-transform:translate3d(0, 150%, 0) scale3d(1, 1, 1) rotateX(90deg) rotateY(null) rotateZ(30deg) skew(0, 0);transform:translate3d(0, 150%, 0) scale3d(1, 1, 1) rotateX(90deg) rotateY(null) rotateZ(30deg) skew(0, 0);transform-style:preserve-3d;opacity:0" class="text-block preloader-text left">Alexandr</div>
		</div>
		<div class="preloader-right-div">
			<div style="opacity:0;-webkit-transform:translate3d(0, 150%, 0) scale3d(1, 1, 1) rotateX(90deg) rotateY(0) rotateZ(30deg) skew(0, 0);-moz-transform:translate3d(0, 150%, 0) scale3d(1, 1, 1) rotateX(90deg) rotateY(0) rotateZ(30deg) skew(0, 0);-ms-transform:translate3d(0, 150%, 0) scale3d(1, 1, 1) rotateX(90deg) rotateY(0) rotateZ(30deg) skew(0, 0);transform:translate3d(0, 150%, 0) scale3d(1, 1, 1) rotateX(90deg) rotateY(0) rotateZ(30deg) skew(0, 0);transform-style:preserve-3d" class="text-block preloader-text right">Bashinsky<span class="text-gradient">.</span></div>
		</div>
	</div>
	<div data-collapse="medium" data-animation="default" data-duration="400" data-easing="ease" data-easing2="ease" role="banner" class="navbar w-nav">
		<div class="wrapper nav">
			<a href="index.html" aria-current="page" class="nav-brand w-nav-brand w--current"><img loading="lazy" src="../images/alexbash.png" alt="" class="image contain" /></a>
			<div class="nav-menu-button w-nav-button">
				<div class="w-icon-nav-menu"></div>
			</div>
		</div>
	</div>
	<div class="home-hero-section wf-section">
		<div class="wrapper home-hero">
			<div class="home-hero-content-wrapper">
				<div data-w-id="74ab6724-2405-4ff7-5feb-8de41757a898" style="-webkit-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="home-hero-heading-wrapper">
					<h1 class="heading">Hi , I &#x27;m<br/></h1>
					<h1 class="heading"><span class="hero-gradient-heading">Alexandr Bashinsky</span><br/></h1></div>
				<div data-w-id="5f6b8c40-fd45-ddd4-d6bd-50e7314945db" style="opacity:0;-webkit-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="home-hero-paragraph-wrapper">
					<p class="paragraph _22px">I’m a serial entrepreneur, tech enthusiast, and a columnist. Currently building CloudBrands, Dubai based family of delivery only restaurant brands
						<br/>
					</p>
				</div>
				<div data-w-id="cc9256c9-2126-550f-e39e-9f0958a35d4d" style="opacity:0;-webkit-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 50%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="home-hero-button-wrapper">
					<a data-w-id="8d024dab-4d5c-3ccf-6983-e2d7818139f6" href="https://twitter.com/abashinsky" target="_blank" class="home-hero-link-wrapper w-inline-block">
						<div class="home-hero-link-text">Let's connect</div>
						<div class="home-hero-arrow-wrapper"><img src="../images/638575c8e36e1b8541ab010c_Vector.svg" loading="lazy" alt="" class="image contain" /></div>
					</a>
					<a data-w-id="10c34f1a-8741-2aeb-ff47-0fa64ec8caa0" href="../webflow.com/index.html" target="_blank" class="home-hero-link-wrapper w-inline-block">
						<div class="home-hero-arrow-wrapper"></div>
					</a>
					<a data-w-id="c5e1e33c-7a4d-1b3e-8454-ff5900c80c50" href="../webflow.com/index.html" target="_blank" class="home-hero-link-wrapper w-inline-block">
						<div class="home-hero-arrow-wrapper"></div>
					</a>
				</div>
			</div>
			<div class="home-hero-image-wrapper"><img src="../images/profile2x.png" loading="lazy" style="-webkit-transform:translate3d(0, 0, 0) scale3d(1.5, 1.5, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0, 0) scale3d(1.5, 1.5, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0, 0) scale3d(1.5, 1.5, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0, 0) scale3d(1.5, 1.5, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" data-w-id="04a5ab51-dab9-48ff-8b93-394963128759" sizes="(max-width: 767px) 90vw, (max-width: 991px) 65vw, (max-width: 1279px) 36vw, (max-width: 1439px) 35vw, 500.0000305175781px" srcset="../images/profile2x.png 500w, ../images/profile2x.png 800w, ../images/profile2x.png 1080w, ../images/profile2x.png 1176w" alt="" class="image home-hero" /></div>
		</div>
	</div>
	<div id="about" class="services-section wf-section">
		<div class="wrapper services _1">
			<div class="service-wrapper">
				<h6 class="custom">👋</h6>
				<h6> About me</h6>
				<h2 class="heading-8">Who am I?</h2>
				<div class="service-pointer-wrapper">
					<div class="service-pointer-item">
						<div class="service-pointer">
							<div class="service-pointer-trigger-text-wrapper">
								<div class="service-pointer-trigger-text">I started my entrepreneurship journey in 2013 as a founder of marketing software startup Picreel, which helps eCommerce and media publishing businesses increase conversion rates on their websites. Within few years, it became the leading CRO marketing software with 1000+ B2B customers including Gartner, Forbes, Rocket Internet, and eventually was acquired by a private equity company.
									<br/>
									<br/>In 2021 I moved to Dubai to build CloudBrands - the most delicious house of cloud-native restaurant brands in MENA region. We are on a mission to accelerate the growth of the cloud economy across emerging markets.
									<br/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="Lets-Talk" class="footer-section">
		<div class="wrapper footer">
			<div class="footer-address-and-links-wrapper">
				<div class="footer-address-wrapper">
					<a href="index.html" aria-current="page" class="footer-logo-wrap w-inline-block w--current"><img src="../images/alexbashwhite.png" loading="lazy" alt="" class="footer-logo" /></a>
				</div>
			</div>
			<div class="sub-footer-wrapper">
				<div class="sub-footer-text">© All rights reserved. <a href="../www.conversionflow.co/index.html" target="_blank" class="footer-down-links">‍</a></div>
			</div>
		</div>
	</div>
	<script src="../images/js/jquery-3.5.1.min.dc5e7f18c84d6e.js?site=638575c7e36e1bc9bcab00a5" type="text/javascript"></script>
	<script src="../images/js/webflow.6bca9f90a.js" type="text/javascript"></script>
	<!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
</body>

</html>
